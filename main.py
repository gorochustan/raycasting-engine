import math

#the following is executed only by cpython
# __pragma__('skip')
import numpy as np
from render_cpython import Screen
print('running in cpython!')
# __pragma__('noskip')

#the following is executed only by transcrypt
# __pragma__('ecom')
'''?
import numscrypt_extra as np
print('running in transcrypt!')
?'''
# __pragma__('noecom')

MAP_WIDTH = 24
MAP_HEIGHT = 24

SCREEN_WIDTH = 640
SCREEN_HEIGHT = 480

class Material:
    def __init__(self, colour):
        self.colour = colour

class WorldMap:
    def __init__(self, array, material_mapping):
        self._array = array
        self.material_mapping = material_mapping
        
    def __getitem__ (self, key):
        print('value: ', self._array[key])
        return self.material_mapping[self._array[key]]  #__:opov

world_map_array = np.array([[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
                            [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                            [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                            [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                            [1,0,0,0,0,0,2,2,2,2,2,0,0,0,0,3,0,3,0,3,0,0,0,1],
                            [1,0,0,0,0,0,2,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,1],
                            [1,0,0,0,0,0,2,0,0,0,2,0,0,0,0,3,0,0,0,3,0,0,0,1],
                            [1,0,0,0,0,0,2,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,1],
                            [1,0,0,0,0,0,2,2,0,2,2,0,0,0,0,3,0,3,0,3,0,0,0,1],
                            [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                            [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                            [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                            [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                            [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                            [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                            [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                            [1,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                            [1,4,0,4,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                            [1,4,0,0,0,0,5,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                            [1,4,0,4,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                            [1,4,0,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                            [1,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                            [1,4,4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                            [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]])

nothing = Material(None)
red = Material([255, 0, 0])
green = Material([0, 255, 0])
blue = Material([0, 0, 255])
white = Material([255]*3)
yellow = Material([255, 255, 0])
                            
material_map = {0 : nothing,
                1 : red,
                2 : green,
                3 : blue,
                4 : white,
                5 : yellow}
                
world_map = WorldMap(world_map_array, material_map)

def clamp(val, minimum=-math.inf, maximum=math.inf):
    if val < minimum:
        return minimum
    elif val > maximum:
        return maximum
    else:
        return val   
    
def run(args=None):
    '''run the engine'''
    
    #initialise values (wrap in player class later?)
    pos = np.array([10, 4])  #player coords
    direction = np.array([-1, 0])  #player facing direction
    plane = np.array([0, 0.66])  #camera plane
        
    time = 0 
    prev_time = 0
    
    screen = Screen(SCREEN_WIDTH, SCREEN_HEIGHT)
    
    while True:  #begin the gameloop
    
        #render the wall in strips
        for strip_x in range(screen.width):
            camera_x = 2 * strip_x / screen.width - 1  #x-coord in camera space

            # __pragma__('opov')
            
            ray_dir = direction + plane * camera_x
            delta_dist = np.abs(1 / ray_dir)  #length of ray from one x or y-side to next x or y-side

            #calculate step and initial sideDist
            step = np.sign(ray_dir)
            
            map_coords = np.array(pos)  #the coords of the ray, initialised to the player's position
            
            if step[0] < 0:
                side_dist_x = pos[0] - map_coords[0]
            else:
                side_dist_x = map_coords[0] - pos[0] + 1
                
            if step[1] < 0:
                side_dist_y = pos[1] - map_coords[1]
            else:
                side_dist_y = map_coords[1] - pos[1] + 1
                
            side_dist = np.array([side_dist_x, side_dist_y]) * delta_dist
            
            #perform DDA
            while True:
                side = np.argmin(side_dist)
                side_dist[side] += delta_dist[side]
                map_coords[side] += step[side]
                
                #check if ray has hit a wall
                print('coords: ', map_coords)
                if world_map[tuple(map_coords.tolist())] != nothing:
                    break
                    
            #calculate distance projected on camera direction
            perp_wall_dist = ((map_coords - pos + (1 - step) / 2) / ray_dir)[side]
            
            #calculate the height of the line to draw on the screen
            line_height = screen.height / perp_wall_dist
            
            #calculate lowest and highest pixel to fill in current stripe
            draw_start = clamp((screen.height - line_height)/2, minimum=0)
            draw_end = clamp((screen.height + line_height)/2, maximum=screen.height)
            
            #get wall material
            material = world_map[tuple(map_coords.tolist())]
            
            #give x and y sides different brightness
            brightness = 1 if side == 0 else 0.5
            
            #draw the pixels of the stripe as a vertical line
            screen.draw_line(strip_x, draw_start, draw_end, material, brightness)
            
            # __pragma__('noopov')
            
        #for testing! show the generated image then exit
        screen.debug_save()
        break
        
    print('finished!')
   
if __name__ == '__main__':
    run()
            
