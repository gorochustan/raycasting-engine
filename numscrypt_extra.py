from numscrypt import *
import math

_builtin_abs = abs

def abs(array):
    '''calculate the absolute value of array element-wise'''
    
    result = empty(array.shape, array.dtype)
    for x in range(array.size):
        result.realbuf[x] = _builtin_abs(array.realbuf[x])
        
    return result
    
def sign(array):
    '''return an element-wise indication if the signs of the numbers in array'''
    
    result = empty(array.shape, array.dtype)
    for x in range(array.size):
        if array.realbuf[x] > 0:
            sign_value = 1
        elif array.realbuf[x] < 0:
            sign_value = -1
        else:
            sign_value = 0
        result.realbuf[x] = sign_value
        
    return result
