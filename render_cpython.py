from PIL import Image, ImageDraw

class Screen:
    def __init__(self, width, height):
        self.width = width
        self.height = height
        
        self._image = Image.new('RGB', (self.width, self.height))
        self._draw = ImageDraw.Draw(self._image)
        
    def draw_line(self, x_pos, y_start, y_end, material, brightness):
        x_pos = int(x_pos)
        colour = tuple(int(val * brightness) for val in material.colour)
        for y_pos in range(int(y_start), int(y_end)+1):
            self._draw.point((x_pos, y_pos), colour)
            
    def debug_save(self):
        self._image.save('test_render.png', 'PNG')
        print('saved!')
